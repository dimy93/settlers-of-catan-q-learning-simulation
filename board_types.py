from enum import Enum
from random import sample

class GridPosition(object):
	def __init__( self , x, y ):
		if not isinstance( x, int ):
			raise ValueError( "First coordinate is not integer" )
		if not isinstance( y, int ):
			raise ValueError( "Second coordinate is not integer" )
		self.x = x
		self.y = y

	def __eq__( self, other ):
		if isinstance( other, self.__class__ ):
			return self.x == other.x and  self.y == other.y
		else:
			return False

	def __add__( self, other ):
		x = self.x + other.x
		y = self.y + other.y
		grid_position_type = type( self )
		return grid_position_type( x, y )

	def __mul__( self, sc ):
		x = self.x * sc
		y = self.y * sc
		grid_position_type = type( self )
		return grid_position_type( x, y )

	def __floordiv__( self, sc ):
		if isinstance( self, GridPositionHex ):
			raise ValueError( "Don't divide Hex positions" )
		x = self.x // sc 
		y = self.y // sc
		grid_position_type = type( self )
		return grid_position_type( x, y )

	def __mod__( self, sc ):
		x = self.x % sc
		y = self.y % sc
		grid_position_type = type( self )
		return grid_position_type( x, y )

	def __hash__( self ):
		return hash( ( self.x, self.y ) )

	def __str__( self ):
		return "("  + str( self.x ) + ", " + str( self.y ) + ")" 

	def __repr__( self ):
		return "("  + str( self.x ) + ", " + str( self.y ) + ")" 


class GridPositionHex( GridPosition ):

	def __init__( self , x, y ):
		super().__init__( x, y )
		if ( not self.x % 6 == 0 ) or ( not self.y % 6 == 0 ):
			raise ValueError( "Invalid Hex positions" )

	def sq_left( self ):
		return self + GridPosition( 0, -6 )

	def sq_right( self ):
		return self + GridPosition( 0, 6 )

	def sq_top_left( self ):
		return self + GridPosition( 6, -6 )

	def sq_top_right( self ):
		return self + GridPosition( 6, 0 )

	def sq_down_left( self ):
		return self + GridPosition( -6, 0 )

	def sq_down_right( self ):
		return self + GridPosition( -6, 6 )

	def top_right_corner( self ):
		return GridPositionCorner( 2, 2 ) + self

	def top_left_corner( self ):
		return GridPositionCorner( 2, -4 ) + self

	def top_corner( self ):
		return GridPositionCorner( 4, -2 ) + self

	def down_right_corner( self ):
		return GridPositionCorner( -2, 4 ) + self

	def down_left_corner( self ):
		return GridPositionCorner( -2, -2 ) + self

	def down_corner( self ):
		return GridPositionCorner( -4, 2 ) + self

	def get_all_corners( self ):
		return [ self.top_right_corner(), self.top_corner(), self.top_left_corner(), \
		         self.down_left_corner(), self.down_corner(), self.down_right_corner() ]

	def common_corners( self, other ):
		return frozenset( self.get_all_corners() ).intersection( frozenset( other.get_all_corners() ) )

class GridPositionCorner( GridPosition ):
	directions_to_closest_hexes = [ GridPosition( 2, 2 ), GridPosition( -2, -2 ), \
	                                GridPosition( 4, -2 ), GridPosition( -4, 2 ), \
	                                GridPosition( -2, 4 ), GridPosition( 2, -4 ) ]

	def get_neighbour_hexes( self ):
		hexes = []
		for dir in GridPositionCorner.directions_to_closest_hexes:
			possible_hex = dir + self
			mod =  possible_hex % 6 
			if mod.x == 0 and mod.y == 0: 
				hexes.append( GridPositionHex( 0, 0 ) + possible_hex )
		return hexes

	def get_neighbour_corners( self ):
		hexes = self.get_neighbour_hexes()
		set_self = frozenset( [ self ] )
		n1 = hexes[ 0 ].common_corners( hexes[ 1 ] ).difference( set_self )
		n2 = hexes[ 1 ].common_corners( hexes[ 2 ] ).difference( set_self )
		n3 = hexes[ 0 ].common_corners( hexes[ 2 ] ).difference( set_self )
		return [ n1, n2, n3 ]

class HexType(Enum):
	SEA = 0
	HARBOR_GENERAL = 1
	HARBOR_GRAIN = 2
	HARBOR_BRICK = 3
	HARBOR_ORE = 4
	HARBOR_LUMBER = 5
	HARBOR_WOOL = 6
	LAND_DESERT = 7
	LAND_GRAIN = 8
	LAND_BRICK = 9
	LAND_ORE = 10
	LAND_LUMBER = 11
	LAND_WOOL = 12

class Game:
	hexes = []
	players = []
	positions = []
	robber_pos = None
	tokens = []
	grid_row_size = [ 4, 5, 6, 7, 6, 5, 4 ]
	avail_tokens = [ 2, 3, 3, 4, 4, 5, 5, 6, 6, 8, 8, 9, 9, 10, 10, 11, 11, 12 ]
	avail_hex_type = [ HexType.LAND_DESERT, \
	                   HexType.LAND_GRAIN, HexType.LAND_GRAIN, HexType.LAND_GRAIN, HexType.LAND_GRAIN, \
	                   HexType.LAND_BRICK, HexType.LAND_BRICK, HexType.LAND_BRICK, \
	                   HexType.LAND_ORE, HexType.LAND_ORE, HexType.LAND_ORE, \
	                   HexType.LAND_LUMBER, HexType.LAND_LUMBER, HexType.LAND_LUMBER, HexType.LAND_LUMBER, \
	                   HexType.LAND_WOOL, HexType.LAND_WOOL, HexType.LAND_WOOL, HexType.LAND_WOOL ]
	def __init__( self ):
		self.hexes = []
		self.players = []
		self.positions = []
		self.robber_pos = None
		self.tokens = []
	def setup_rng_board( self ):
		self.tokens = random.sample( Game.avail_tokens, len( avail_tokens ) )
		hex_types = random.sample( Game.avail_hex_type, len( avail_hex_type ) )
		desert_pos = hex_types.index( HexType.LAND_DESERT )
		self.tokens.insert( desert_pos, 7 )
		for ( token, hex_type ) in zip( self.tokens, hex_types ):
			self.hexes.append( Hex( self, token, hex_type ) )
		self.robber_pos = self.hexes[desert_pos].id


class Hex:
	current_id = 0;
	def __init__(self, game, token, type, id=None):
		if id:
			self.id = id
		else:
			self.id = current_id
			current_id = current_id + 1
		self.token = token
		self.type = type
		self.game = game

class BuildingType(Enum):
	NOTHING = 0
	ROAD = 1
	SETTLEMENT = 2
	CITY = 3

class Position:
	current_id = 0;
	def __init__(self, game, adj_hexes, id=None):
		if id:
			self.id = id
		else:
			self.id = current_id
			current_id = current_id + 1
		self.adj_hexes = adj_hexes
		self.building = BuildingType.NOTHING
		self.owned = -1
		self.game = game

	def add_building(self, type, player_id):
		self.building = type
		self.owned = self.game.players[player_id]

	def check_if_hit(self, dice_val):
		hits = []
		for hex_id in adj_hexes:
			if not self.game.robber_pos == hex_id:
				if self.game.hexes[hex_id].dice_val == dice_val:
					hits.append(hex_id)
		return hits
