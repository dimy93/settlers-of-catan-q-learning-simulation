from setuptools import setup, find_packages
from codecs import open
from os import path
from pip.req import parse_requirements

here = path.abspath(path.dirname(__file__))
install_reqs = parse_requirements(here + '/requirements.txt',
                                 session=True)
reqs = [str(ir.req) for ir in install_reqs]

# Obtain description from readme
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='settlers-of-catan-q-learning-simulation',
    version='1.0',
    description='Q-Learning project',
    long_description=long_description,
    url=('https://franciskv@bitbucket.org/dimy93'+
         '/settlers-of-catan-q-learning-simulation.git'),
    author='Dimitar Dimitrov, Francisco Vargas',
    author_email='vargfran@gmail.com',
    license='MIT',

    classifiers=[

        'Development Status :: 3 - Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: Developers, Users',
        'Topic :: Games, Catan, RL, Q-Learning',

        # Pick your license as you wish (should match "license" above)
        'License :: OSI Approved :: MIT License',

        # Specify the Python versions you support here. In particular,
        # ensure that you indicate whether you support Python 2, 
        # Python 3 or both.
        'Programming Language :: Python :: 3.4',

    ],

    keywords='maps random probability open data',

    packages=find_packages(exclude=[ 'test']),

    install_requires=reqs,
)