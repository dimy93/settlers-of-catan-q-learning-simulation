from enum import Enum

class Game:
	hexs = []
	players = []
	positions = []
	robber_pos = None

class HexType(Enum):
	SEA = 0
	HARBOR_GENERAL = 1
	HARBOR_GRAIN = 2
	HARBOR_BRICK = 3
	HARBOR_ORE = 4
	HARBOR_LUMBER = 5
	HARBOR_WOOL = 6
	LAND_DESERT = 7
	LAND_GRAIN = 8
	LAND_BRICK = 9
	LAND_ORE = 10
	LAND_LUMBER = 11
	LAND_WOOL = 12

class Hex:
	current_id = 0;
	def __init__(self, dice_val, type, id=None):
		if id:
			self.id = id
		else:
			self.id = current_id
			current_id = current_id + 1
		self.dice_val = dice_val
		self.type = type

class BuildingType(Enum):
	NOTHING = 0
	ROAD = 1
	SETTLEMENT = 2
	CITY = 3

class Position:
	current_id = 0;
	def __init__(self, adj_hexs, id=None):
		if id:
			self.id = id
		else:
			self.id = current_id
			current_id = current_id + 1
		self.adj_hexs = adj_hexs
		self.building = BuildingType.NOTHING
		self.owned = -1

	def add_building(self, type, player):
		self.building = type
		self.owned = player.id

	def check_if_hit(self, dice_val):
		hits = []
		for hex_id in adj_hexs:
			if not robber_pos == hex_id:
				if hexs[hex_id].dice_val == dice_val:
					hits.append(hex_id)
		return hits

