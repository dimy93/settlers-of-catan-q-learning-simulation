# Settlers of Catan Simulator for AI learning

## Installation

Requires python3-pip and python3-setuptools prior to running command in venv (within root dir of project):

```
(venv) pip install -e .
```

Will install the package from local dir  and reflect local changes.